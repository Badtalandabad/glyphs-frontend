export default class AppMessage {

  public text: string;

  public type: AppMessageType;

  constructor(text: string, type: AppMessageType) {
    this.text = text;
    this.type = type;
  }
}

export enum AppMessageType {
  Success = 'success',
  Warning = 'warning',
  Error = 'error',
}