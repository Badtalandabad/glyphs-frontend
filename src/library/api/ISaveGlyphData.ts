export interface ISaveGlyphData {
  newGlyph: string,
  pinyin: string,
  meaning: string,
}