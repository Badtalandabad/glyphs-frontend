import {ISaveGlyphData} from "./ISaveGlyphData";
import ApiResponse from "./ApiResponse";
import Glyph from "@/library/Glyph";

class Api {
  public readonly BASE_URL = 'http://glyphs.local/api/v1/';
  public readonly GET_GLYPHS_ACTION = 'get-glyphs';
  public readonly SAVE_GLYPH_ACTION = 'save-glyph';

  /**
   * Does request to API
   *
   * @param actionName     Constant, action name
   * @param data           Data to send to server (empty object if no data needed)
   */
  public callApi<REQ_T>(actionName: string, data: REQ_T): Promise<ApiResponse<Record<string, unknown>>> {
    const endpoint = this.BASE_URL + actionName;

    const params = {
      method: 'POST',
      mode: 'cors' as RequestMode,
      headers: {'Content-Type': 'application/json',},
      body: JSON.stringify(data),
    };

    return fetch(endpoint, params).then((response) => {
      if (!response.ok) {
        throw new Error('API responded with an error');
      }

      return response.json().then(resultObj => {
        if (typeof resultObj.message !== 'string'
            || typeof resultObj.data !== 'object'
        ) {
          throw new Error('Response format is incorrect');
        }

        return new ApiResponse(response.status, resultObj.message, resultObj.data);
      });
    });
  }

  public getGlyphs(glyphsCount = 100): Promise<ApiResponse<Glyph[]>> {
    return this.callApi(this.GET_GLYPHS_ACTION, {glyphsCount})
        .then(result => {
          if (!Array.isArray(result.data)) {
            throw new Error('Response format from get-glyph request is incorrect');
          }
          const data = Glyph.makeGlyphsFromRawData(result.data)
          return new ApiResponse(result.status, result.message, data);
        });
  }

  public saveGlyph(data: ISaveGlyphData): Promise<ApiResponse<Empty>> {
    return this.callApi(this.SAVE_GLYPH_ACTION, data);
  }

}

export default new Api();