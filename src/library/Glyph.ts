export default class Glyph {
  public static makeGlyphsFromRawData(data: Record<string, unknown>[]): Glyph[] {
    return data.map(value => {
      if (typeof value.symbol !== 'string'
          || typeof value.pinyin !== 'string'
          || typeof value.meaning !== 'string'
      ) {
        throw new Error('Data format is incompatible with glyph class');
      }
      return new Glyph(value.symbol, value.pinyin, value.meaning)
    });
  }

  private _symbol = '';

  public get symbol(): string {
    return this._symbol;
  }

  public set symbol(value: string) {
    if (value.length > 1) {
      throw Error('Glyph symbol must be just one symbol');
    }
    this._symbol = value;
  }

  public pinyin: string[];

  public meaning: string;

  constructor(symbol: string, pinyin: string, meaning: string) {
    this.symbol = symbol;
    this.pinyin = pinyin.split(/(\s+)/).filter( e => e.trim().length > 0);
    if (!this.pinyin.length) {
      this.pinyin = ['-'];
    }
    this.meaning = meaning;
  }

  public getOnePinyin(): string {
    return this.pinyin[0];
  }

  public getAllPinyin(): string {
    return this.pinyin.join(' ');
  }
}