import {createStore} from 'vuex'
import AppMessage, {AppMessageType} from '@/library/AppMessage';
import Glyph from '@/library/Glyph';
import api from '@/library/api';

export interface State {
  appMessage?: AppMessage,
  glyphs: Glyph[],
  isLoading: boolean,
}

export const store = createStore<State>({
  state: {
    appMessage: undefined,
    glyphs: [],
    isLoading: false,
  },
  mutations: {
    setIsLoading(state, isLoading: boolean) {
      state.isLoading = isLoading
    },
    setGlyphs(state, glyphs: Glyph[]) {
      state.glyphs = glyphs;
    },
    setAppMessage(state, payload: {text: string, type: AppMessageType}) {
      state.appMessage = new AppMessage(payload.text, payload.type);
    },
    removeAppMessage(state) {
      state.appMessage = undefined;
    },
  },
  actions: {
    initData({commit}) {
      commit('setIsLoading', true);
      api.getGlyphs().then(result => {
        commit('setGlyphs', result.data);
        commit('setIsLoading', false);
      }).catch((error: Error) => {
        console.error(error.message);
        commit('setAppMessage', {text: 'Network error', type: AppMessageType.Error});
      }).finally(() => commit('setIsLoading', false));
    },
  },
  modules: {
  }
})
